import React, { useState } from 'react';
import './MessageInput.css';

const MessageEdit = (props) => {
  const [text, setText] = useState(props.message.text);
  const handleEditMessage = (event) => {
    event.preventDefault();
    props.editMessage(text, props.message.id);
    setText('');
  }
  return (
    <form onSubmit={ev => handleEditMessage(ev)} className="Message-input">
      <textarea
        onChange={ev => setText(ev.target.value)}
        value={text}
        placeholder="Type your message and hit ENTER"
        type="text" />
      <div class="Input-button">
        <button type="submit" >Edit</button>
      </div>
    </form>
  );
}



export default MessageEdit;
