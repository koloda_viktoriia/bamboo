import React, { useState } from 'react';
import './MessageInput.css';

const MessageInput = (props) => {
  const [text, setText] = useState('');
  const handleAddMessage = (event) => {
    event.preventDefault();
    props.addMessage(text);
    setText('');
  }

  return (
    <form onSubmit={ev => handleAddMessage(ev)} className="Message-input">
      <textarea
        onChange={ev => setText(ev.target.value)}
        value={text}
        placeholder="Type your message and click Send"
        type="text" />
      <div class="Input-button">
        <button type="submit" >Send</button>
      </div>
    </form>
  );
}


export default MessageInput;
