import React, { useState, useEffect, useReducer } from 'react';
import Header from '../Header/Header';
import ChatHeader from '../ChatHeader/ChatHeader';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';

import MessageEdit from '../MessageInput/MessageEdit';
import Footer from '../Footer/Footer';
import data from '../../data.json';
import userProfile from '../../user.json';
import './Chat.css';
import logo from './logo.svg';

import { v4 as uuidv4 } from 'uuid'

const Chat = () => {
  const [messages, setMessages] = useState(data);
  const [editedMessage, setEditedMessage] = useState(undefined);
  const [likes, setLikes] = useState([]);
  const [isLoading, setisLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => setisLoading(false), 500);
  });


  messages.sort(
    (a, b) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
    });
  const addMessage = (text) => {
    const message = {
      id: uuidv4(),
      userId: userProfile.userId,
      avatar: userProfile.avatar,
      user: userProfile.user,
      text: text,
      createdAt: new Date().getTime(),
      updatedAt: ""
    }
    const messageList = [...messages, message];
    setMessages(messageList);
  }

  const editMessage = (text, id) => {
    const editedMessage = (message) => (
      {
        ...message,
        text,
        updatedAt: new Date().getTime()
      });
    setMessages(messages
      .map(message =>
        message.id === id ? editedMessage(message) : message));

    setEditedMessage(undefined);

  }
  const deleteMessage = (id) => {
    setMessages(messages.filter(message => message.id !== id));
  }


  const showEditInput = (id) => {
    const message = messages.find(message => message.id === id);
    setEditedMessage(message);
  }

  const liked = (userId, id, isLike) => {
    if (isLike) {
      setLikes(likes.filter(like => like.id !== id & like.userId !== id));
    } else {
      const newLike = { userId, id };
      const likeList = [...likes, newLike];
      setLikes(likeList);
    }
  }

  return (
    isLoading
      ?
      <div class="Spinner">
        <img src={logo} className="Spinner-logo" alt="logo" />
      </div>
      :
      <>
        <Header />
        <div className="Chat">
          <ChatHeader
            data={messages}
          />
          <MessageList data={messages}
            deleteMessage={deleteMessage}
            ShowEditInput={showEditInput}
            likes={likes}
            userId={userProfile.userId}
            liked={liked} />

          {editedMessage === undefined ? <MessageInput addMessage={addMessage} /> :
            <MessageEdit message={editedMessage} editMessage={editMessage} />}
        </div>
        <Footer />
      </>
  );

}

export default Chat;
